@ObservedV2
export class DragStyle implements AttributeModifier<TextAttribute> {
  // 当前 X轴 偏移量
  currentOffsetX: number = 0;
  // 当前 Y轴 偏移量
  currentOffsetY: number = 0;
  // 当前 放大倍数
  currentScale: number = 0;
  // 存储计算数据
  // 存储 “元素交换时” “手指” 位置
  fingerOffsetX: number = 0;
  fingerOffsetY: number = 0;
  // 元素交换时存储元素当前的位置
  postMovePositionX: number = 0;
  postMovePositionY: number = 0;
  // 存储 新位置 和 旧位置的 差值
  positionDiffX: number = 0;
  positionDiffY: number = 0;

  // 更改样式
  applyNormalAttribute(instance: TextAttribute): void {
    instance.scale({
      x: this.currentScale,
      y: this.currentScale
    });

    instance.translate({
      x: this.currentPosition.x as number,
      y: this.currentPosition.y as number
    })
  }

  // 获取当前元素的位置
  get currentPosition(): Position {
    let x = this.currentOffsetX - this.fingerOffsetX + this.positionDiffX;
    let y = this.currentOffsetY - this.fingerOffsetY + this.positionDiffY;
    return {
      x: x,
      y: y
    }
  }

  // 重置数据
  reset(){
    this.currentOffsetX = 0;
    this.currentOffsetY = 0;
    this.currentScale = 0;
    this.fingerOffsetX = 0;
    this.fingerOffsetY = 0;
    this.postMovePositionX = 0;
    this.postMovePositionY = 0;
    this.positionDiffX = 0;
    this.positionDiffY = 0;
  }
}